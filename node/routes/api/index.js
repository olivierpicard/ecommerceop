const express = require("express");
const router = express.Router();

router.use("/users", require("./users"));
router.use("/stock", require("./stock"));
router.use("/stock", require("./stock"));

module.exports = router;
