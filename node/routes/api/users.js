const mongoose = require("mongoose");
const passport = require("passport");
const router = require("express").Router();
const auth = require("../auth");
const Users = mongoose.model("Users");

// POST new user route (optional, everyone has access)
router.post("/signup", auth.optional, (req, res, next) => {
  const {
    body: { user }
  } = req;

  if (
    !user.username ||
    !user.firstname ||
    !user.lastname ||
    !user.email ||
    !user.password
  ) {
    return res.sendStatus(422);
  }

  const finalUser = new Users(user);
  finalUser.setPassword(user.password);
  return finalUser
    .save()
    .then(() => res.json(finalUser.toAuthJSON()))
    .catch(() => res.status(500).json({ email: true }));
});

//POST login route (optional, everyone has access)
router.post("/login", auth.optional, (req, res, next) => {
  const {
    body: { user }
  } = req;

  if (!user.email || !user.password) return res.sendStatus(422);

  return passport.authenticate(
    "local",
    { session: false },
    (err, passportUser, info) => {
      if (err) return next(err);
      if (passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();
        return res.json(user.toAuthJSON());
      }
      return res.sendStatus(400);
    }
  )(req, res, next);
});

// GET current route (required, only authenticated users have access)
router.get("/current", auth.required, (req, res, next) => {
  const {
    payload: { id }
  } = req;

  return Users.findById(id).then(user => {
    if (!user) return res.sendStatus(400);
    return res.json(user.toAuthJSON());
  });
});

router.delete("/preparetests", auth.optional, (req, res, next) => {
  Users.findOneAndDelete({ username: "__username_usertest" })
    .then(() => res.sendStatus(200))
    .catch(err => res.status(500).json(err));
});

module.exports = router;
