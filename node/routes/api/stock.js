const mongoose = require("mongoose");
const passport = require("passport");
const router = require("express").Router();
const auth = require("../auth");
const Stock = mongoose.model("Stock");
const PurchaseHistory = mongoose.model("PurchaseHistory");

/**
 * GET global bestseller
 * @var limite select the number of item to return
 * */
router.post("/bestseller", auth.optional, (req, res, next) => {
  const itemCount = req.body.limit;
  Stock.find()
    .sort({ quantitySold: -1 })
    .limit(itemCount)
    .then(val => res.send(val))
    .catch(() => res.sendStatus(404));
});

/**
 * GET bestseller with this type
 * @var limite select the number of item to return
 * @var type item type to select
 * */
router.post("/bestsellerbytype", auth.optional, (req, res, next) => {
  const itemCount = req.body.limit;
  const type = req.body.type;

  Stock.find({ type: type })
    .sort({ quantitySold: -1 })
    .limit(itemCount)
    .then(val => res.json(val))
    .catch(() => res.sendStatus(404));
});

/// GET new arriving in stock
router.post("/latest", auth.optional, (req, res, next) => {
  const itemCount = req.body.limit;
  Stock.find()
    .sort({ releaseDate: -1 })
    .limit(itemCount)
    .then(val => res.json(val))
    .catch(() => res.sendStatus(404));
});

/// GET newers item by their types
router.post("/lastestbytype", auth.optional, (req, res, next) => {
  const itemCount = req.body.limit;
  const type = req.body.type;

  Stock.find({ type: type })
    .sort({ releaseDate: -1 })
    .limit(itemCount)
    .then(val => res.json(val))
    .catch(() => res.sendStatus(404));
});

router.post("/findbyreference", auth.optional, (req, res, next) => {
  const ref = req.body.reference;
  Stock.findOne({ reference: ref })
    .then(val => res.json(val))
    .catch(() => res.sendStatus(404));
});

router.post("/findbyid", auth.optional, (req, res, next) => {
  Stock.findById(req.body.id)
    .then(val => res.json(val))
    .catch(() => res.sendStatus(404));
});

/**
 * @description POST/ add an item to stock.
 * @returns produce an error if this item already exist, else it send status 200
 */
router.post("/add", auth.required, fAdd);

/// PUT update an item's field
router.put("/update", auth.required, fUpdate);

/// DELETE delete a field
router.delete("/remove", auth.required, (req, res, next) => {
  const id = req.body.id;
  Stock.remove({ _id: id })
    .then(() => res.sendStatus(200))
    .catch(() => res.sendStatus(404));
});

router.delete("/preparetests", auth.optional, (req, res, next) => {
  Stock.deleteMany({ type: /^_/ })
    .then(() => res.sendStatus(200))
    .catch(err => res.status(500).send(err));
});

// router.post("/order", auth.required, (req, res, next) => {

// });

///
/// Fonction Séparer
///

function fAdd(req, res, next) {
  const inputItem = req.body;
  const stock = new Stock(inputItem);

  if (!stock.validateItem(inputItem)) return res.sendStatus(422);

  stock
    .save()
    .then(() => res.send(stock.toJSON()))
    .catch(err => res.status(500).send(err));
}

function fUpdate(req, res, next) {
  const filter = req.body.filter;
  const update = req.body.update;

  Stock.findOneAndUpdate(filter, update)
    .then(() => res.sendStatus(200))
    .catch(() => res.sendStatus(422));
}

module.exports = router;
