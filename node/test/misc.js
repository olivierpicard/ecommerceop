module.exports = {
  fakeUser: {
    user: {
      username: "__username_usertest",
      firstname: "__firstname_usertest",
      lastname: "__lastname_usertest",
      email: "mailtest@usertest.com",
      password: "__password__usertest"
    }
  },

  fakeObjects: [
    {
      // 0
      type: "_keyboard",
      brand: "logitech",
      reference: "LGT-X2034_FR",
      releaseDate: new Date("2017-01-12").toISOString(),
      imageURL: "KEY_1.jpg",
      price: 33.2,
      quantityInStock: 1220,
      quantitySold: 20,
      spec: {
        english: [{ name: "layout", value: "french" }, { nargbLights: "yes" }]
      }
    },

    {
      // 1
      type: "_keyboard",
      brand: "microsoft",
      reference: "MCSFT-FR22X",
      releaseDate: new Date("2017-04-12").toISOString(),
      imageURL: "KEY_2.jpg",
      price: 60.2,
      quantityInStock: 4000,
      quantitySold: 5000,
      spec: {
        english: [{ name: "layout", value: "french" }, { nargbLights: "yes" }]
      }
    },

    {
      // 2
      type: "_processor",
      brand: "intel",
      reference: "i7-7770",
      imageURL: "PROC_1.jpg",
      releaseDate: new Date("2017-11-12").toISOString(),
      price: 7000,
      quantityInStock: 7000,
      quantitySold: 4000,
      spec: {
        english: [
          { name: "core", value: 4 },
          { name: "thread", value: 8 },
          { name: "frequency", value: "3.6 Ghz" },
          { name: "turbo boost frequency", value: "4.1 Ghz" },
          { name: "TDP", value: "95 W" }
        ]
      }
    },

    {
      // 3
      type: "_processor",
      brand: "intel",
      reference: "i7-7010",
      releaseDate: new Date("2018-01-12").toISOString(),
      imageURL: "PROC_1.jpg",
      price: 5500,
      quantityInStock: 80,
      quantitySold: 7000,
      spec: {
        english: [
          { name: "core", value: 4 },
          { name: "thread", value: 8 },
          { name: "frequency", value: "3.6 Ghz" },
          { name: "turbo boost frequency", value: "4.1 Ghz" },
          { name: "TDP", value: "95 W" }
        ]
      }
    },

    {
      // 4
      type: "_processor",
      brand: "intel",
      reference: "i9-9600K",
      releaseDate: new Date("2019-12-20").toISOString(),
      imageURL: "PROC_5.jpg",
      price: 9500,
      quantityInStock: 40,
      quantitySold: 100,
      spec: {
        english: [
          { name: "core", value: 4 },
          { name: "thread", value: 8 },
          { name: "frequency", value: "3.6 Ghz" },
          { name: "turbo boost frequency", value: "4.1 Ghz" },
          { name: "TDP", value: "95 W" }
        ]
      }
    },

    {
      // 5
      type: "_processor",
      brand: "amd",
      reference: "ryzen5100",
      releaseDate: new Date("2018-02-20").toISOString(),
      imageURL: "PROC_3.jpg",
      price: 370,
      quantityInStock: 50,
      quantitySold: 30,
      spec: {
        english: [
          { name: "core", value: 4 },
          { name: "thread", value: 8 },
          { name: "frequency", value: "3.6 Ghz" },
          { name: "turbo boost frequency", value: "4.1 Ghz" },
          { name: "TDP", value: "95 W" }
        ]
      }
    },

    {
      // 6
      type: "_processor",
      brand: "amd",
      reference: "ryzen5300",
      releaseDate: new Date("2019-12-21").toISOString(),
      imageURL: "PROC_3.jpg",
      price: 450,
      quantityInStock: 60,
      quantitySold: 30,
      spec: {
        english: [
          { name: "core", value: 4 },
          { name: "thread", value: 8 },
          { name: "frequency", value: "3.6 Ghz" },
          { name: "turbo boost frequency", value: "4.1 Ghz" },
          { name: "TDP", value: "95 W" }
        ]
      }
    },

    {
      // 7
      type: "_processor",
      brand: "amd",
      reference: "ryzen7300",
      releaseDate: new Date("2019-12-22").toISOString(),
      imageURL: "PROC_4.jpg",
      price: 530,
      quantityInStock: 70,
      quantitySold: 40,
      spec: {
        english: [
          { name: "core", value: 4 },
          { name: "thread", value: 8 },
          { name: "frequency", value: "3.6 Ghz" },
          { name: "turbo boost frequency", value: "4.1 Ghz" },
          { name: "TDP", value: "95 W" }
        ]
      }
    },

    {
      type: "_graphic_card",
      brand: "amd",
      reference: "radeaon 5700",
      releaseDate: new Date("2018-05-13").toISOString(),
      imageURL: "CG_1.jpg",
      price: 630,
      quantityInStock: 70,
      quantitySold: 40,
      spec: [
        { name: "core", value: 4 },
        { name: "thread", value: 8 },
        { name: "frequency", value: "3.6 Ghz" },
        { name: "turbo boost frequency", value: "4.1 Ghz" },
        { name: "TDP", value: "95 W" }
      ]
    },

    {
      type: "_graphic_card",
      brand: "amd",
      reference: "radeaon 5700X",
      releaseDate: new Date("2017-11-30").toISOString(),
      imageURL: "CG_1.jpg",
      price: 780,
      quantityInStock: 40,
      quantitySold: 20,
      spec: {
        english: [
          { name: "frequency memory video", value: "14000 Mhz" },
          { name: "memory type", value: "GDDR6" },
          { name: "memory size", value: "6 GB" },
          { name: "chipset frequency base", value: "1365 Ghz" },
          { name: "chipset frequency boost", value: "1830 Ghz" },
          { name: "power consumption", value: "190 W" }
        ]
      }
    },

    {
      type: "_graphic_card",
      brand: "amd",
      reference: "radeaon 3600X",
      releaseDate: new Date("2016-08-15").toISOString(),
      imageURL: "CG_2.jpg",
      price: 310,
      quantityInStock: 20,
      quantitySold: 5,
      spec: {
        english: [
          { name: "frequency memory video", value: "14000 Mhz" },
          { name: "memory type", value: "GDDR6" },
          { name: "memory size", value: "6 GB" },
          { name: "chipset frequency base", value: "1365 Ghz" },
          { name: "chipset frequency boost", value: "1830 Ghz" },
          { name: "power consumption", value: "190 W" }
        ]
      }
    },

    {
      type: "_graphic_card",
      brand: "nvidia",
      reference: "gtx 1080",
      releaseDate: new Date("2017-06-23").toISOString(),
      imageURL: "CG_3.jpg",
      price: 560,
      quantityInStock: 80,
      quantitySold: 64,
      spec: {
        english: [
          { name: "frequency memory video", value: "14000 Mhz" },
          { name: "memory type", value: "GDDR6" },
          { name: "memory size", value: "6 GB" },
          { name: "chipset frequency base", value: "1365 Ghz" },
          { name: "chipset frequency boost", value: "1830 Ghz" },
          { name: "power consumption", value: "190 W" }
        ]
      }
    },

    {
      type: "_graphic_card",
      brand: "nvidia",
      reference: "gtx 2080",
      releaseDate: new Date("2019-04-11").toISOString(),
      imageURL: "CG_4.jpg",
      price: 860,
      quantityInStock: 200,
      quantitySold: 110,
      spec: {
        english: [
          { name: "frequency memory video", value: "14000 Mhz" },
          { name: "memory type", value: "GDDR6" },
          { name: "memory size", value: "6 GB" },
          { name: "chipset frequency base", value: "1365 Ghz" },
          { name: "chipset frequency boost", value: "1830 Ghz" },
          { name: "power consumption", value: "190 W" }
        ]
      }
    },

    {
      type: "_graphic_card",
      brand: "nvidia",
      reference: "gtx 2070",
      releaseDate: new Date("2019-05-17").toISOString(),
      imageURL: "CG_4.jpg",
      price: 740,
      quantityInStock: 240,
      quantitySold: 170,
      spec: {
        english: [
          { name: "frequency memory video", value: "14000 Mhz" },
          { name: "memory type", value: "GDDR6" },
          { name: "memory size", value: "6 GB" },
          { name: "chipset frequency base", value: "1365 Ghz" },
          { name: "chipset frequency boost", value: "1830 Ghz" },
          { name: "power consumption", value: "190 W" }
        ]
      }
    }
  ]
};
