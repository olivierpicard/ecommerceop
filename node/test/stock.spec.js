const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const request = require("request");
const { fakeUser, fakeObjects } = require("./misc");
const { serverPort } = require("../misc");

const baseurl = "http://localhost:" + serverPort + "/api/stock/";
const testItem = fakeObjects[1];
var testId;

describe("Test stock API ", function() {
  before(function(done) {
    const userUrl = "http://localhost:" + serverPort + "/api/users/";
    request.post(
      {
        url: userUrl + "login",
        body: fakeUser,
        json: true
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        expect(res.body).to.not.be.null;
        user = res.body;
        return done();
      }
    );
  });

  before(function(done) {
    request.delete({ url: baseurl + "preparetests" }, (err, res, body) => {
      expect(err).to.be.null;
      expect(res.statusCode).to.be.equal(200);
      return done();
    });
  });

  it("Add products to database", function(done) {
    counter = 0;
    const stockSize = fakeObjects.length;

    function runRequest(item) {
      request.post(
        {
          url: baseurl + "add",
          headers: { Authorization: user.token },
          body: item,
          json: true
        },
        (err, res, body) => {
          counter++;
          expect(err).to.be.null;
          expect(res.statusCode).to.be.equal(200);
          if (counter == stockSize) return done();
        }
      );
    }
    fakeObjects.forEach(elem => {
      runRequest(elem);
    });
  });

  it("Duplicate products in database should return error code", function(done) {
    counter = 0;
    const stockSize = fakeObjects.length;

    function runRequest(item) {
      request.post(
        {
          url: baseurl + "add",
          headers: { Authorization: user.token },
          body: item,
          json: true
        },
        (err, res, body) => {
          counter++;
          expect(res.statusCode).to.not.be.equal(200);
          if (counter == stockSize) return done();
        }
      );
    }
    fakeObjects.forEach(elem => {
      runRequest(elem);
    });
  });

  it("Try to update the second item and check if it's correct", function(done) {
    const newref = "_referenceTest";
    function checkUpdate() {
      request.post(
        {
          url: baseurl + "findbyreference",
          json: true,
          body: { reference: newref }
        },
        (err, res, body) => {
          expect(err).to.be.null;
          expect(res.statusCode).to.be.equal(200);
          testItem.reference = newref;
          expect(body).to.be.deep.include(testItem);
          expect(body.id).to.not.be.null;
          testId = body.id;
          return done();
        }
      );
    }
    request.put(
      {
        url: baseurl + "update",
        headers: { Authorization: user.token },
        json: true,
        body: {
          filter: { reference: testItem.reference },
          update: { reference: newref }
        }
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        checkUpdate();
      }
    );
  });

  it("Get the best seller item", function(done) {
    request.post(
      {
        url: baseurl + "bestseller",
        json: true,
        body: { limit: 3 }
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        expect(body[0]).to.be.deep.include(fakeObjects[3]);
        expect(body[1]).to.be.deep.include(fakeObjects[1]);
        expect(body[2]).to.be.deep.include(fakeObjects[2]);
        return done();
      }
    );
  });

  it("Get the best seller by type", function(done) {
    request.post(
      {
        url: baseurl + "bestsellerbytype",
        json: true,
        body: { limit: 3, type: "_processor" }
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        expect(body[0]).to.be.deep.include(fakeObjects[3]);
        expect(body[1]).to.be.deep.include(fakeObjects[2]);
        expect(body[2]).to.be.deep.include(fakeObjects[4]);
        return done();
      }
    );
  });

  it("Get latest items", function(done) {
    request.post(
      {
        url: baseurl + "latest",
        json: true,
        body: { limit: 3 }
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        expect(body[0]).to.be.deep.include(fakeObjects[7]);
        expect(body[1]).to.be.deep.include(fakeObjects[6]);
        expect(body[2]).to.be.deep.include(fakeObjects[4]);
        return done();
      }
    );
  });

  it("Get latest items by type", function(done) {
    request.post(
      {
        url: baseurl + "lastestbytype",
        json: true,
        body: { limit: 3, type: "_processor" }
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        expect(body[0]).to.be.deep.include(fakeObjects[7]);
        expect(body[1]).to.be.deep.include(fakeObjects[6]);
        expect(body[2]).to.be.deep.include(fakeObjects[4]);
        return done();
      }
    );
  });

  it("Find an item by ID", function(done) {
    request.post(
      {
        url: baseurl + "findbyid",
        json: true,
        body: { id: testId }
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        expect(body).to.be.deep.include(testItem);
        return done();
      }
    );
  });
});
