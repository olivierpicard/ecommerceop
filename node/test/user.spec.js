const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const request = require("request");
const { serverPort } = require("../misc");
const { fakeUser } = require("./misc");

var recupUser;
const baseurl = "http://localhost:" + serverPort + "/api/users/";

describe("Test users API and users auth required routes", function() {
  before(function() {
    request.delete({ url: baseurl + "preparetests" }, function(err, res, body) {
      expect(err).to.be.null;
      expect(res.statusCode).to.equal(200);
    });
  });

  it("SignUp should add an user to Database", function(done) {
    request.post(
      {
        url: baseurl + "signup",
        body: fakeUser,
        json: true
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        recupUser = res.body;
        return done();
      }
    );
  });

  it("Should throw error when we try to duplicate an existing user", function(done) {
    request.post(
      {
        url: baseurl + "signup",
        body: fakeUser,
        json: true
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.not.be.equal(200);
        return done();
      }
    );
  });

  it("Retrieved user should match the base user", function() {
    expect(recupUser).to.not.be.null;
    expect(recupUser.token).to.not.be.null;
    expect(recupUser._id).to.not.be.null;
    expect(recupUser.username).to.be.equal(fakeUser.user.username);
    expect(recupUser.firstname).to.be.equal(fakeUser.user.firstname);
    expect(recupUser.lastname).to.be.equal(fakeUser.user.lastname);
    expect(recupUser.email).to.be.equal(fakeUser.user.email);
  });

  it("Login should be equal to retrieved user at signup", function(done) {
    request.post(
      {
        url: baseurl + "login",
        body: fakeUser,
        json: true
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        const result = res.body;
        expect(result).to.be.deep.equal(recupUser);
        done();
      }
    );
  });

  it("Test users route auth", function(done) {
    request.get(
      {
        url: baseurl + "current",
        headers: { Authorization: recupUser.token }
      },
      (err, res, body) => {
        expect(err).to.be.null;
        expect(res.statusCode).to.be.equal(200);
        const result = res.body;
        expect(JSON.parse(result)).to.be.deep.equal(recupUser);
        done();
      }
    );
  });
});
