const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const { mongoConnectionUrl, serverPort } = require("./misc");
const bodyParser = require("body-parser");
var cors = require("cors");

const app = express();
var dir = path.join(__dirname, "public");
app.use(express.static(dir));
app.use(cors({ origin: "http://localhost:4200" }));
app.use(bodyParser.json());
require("./models/db.user");
require("./models/db.stock");
require("./models/db.purchase_history");
require("./config/passport");
app.use(require("./routes"));

mongoose.connect(mongoConnectionUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true
});

app.get("/ping", (req, res) => {
  res.send("pong");
});

app.listen(serverPort);
