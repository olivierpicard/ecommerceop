const mongoose = require("mongoose");
const { Schema } = mongoose;

const PurchaseHistorySchema = new Schema({
  userID: { type: Number, require: true },
  itemID: { type: Number, require: true },
  quantity: { type: Number, require: true }
});

mongoose.model("PurchaseHistory", PurchaseHistorySchema, "PurchaseHistory");
