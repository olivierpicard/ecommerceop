const mongoose = require("mongoose");
const { Schema } = mongoose;

const StockSchema = new Schema({
  type: { type: String, require: true },
  brand: { type: String, require: true },
  reference: { type: String, require: true, unique: true },
  imageURL: { type: String, require: true },
  price: { type: Number, require: true },
  quantityInStock: { type: Number, require: true },
  quantitySold: { type: Number, required: true, default: 0 },
  releaseDate: { type: Date, required: true },
  spec: { type: {} }
});

/**
 * Si l'item en paramètre a un champs requis vide alors
 * il est considéré comme non valide
 */
StockSchema.methods.validateItem = function(item) {
  return !(
    !item.type ||
    !item.brand ||
    !item.reference ||
    !item.price ||
    !item.quantityInStock ||
    !item.quantitySold
  );
};

/**
 * return this model to JSON format
 */
StockSchema.methods.toJSON = function() {
  return {
    id: this._id,
    type: this.type,
    brand: this.brand,
    reference: this.reference,
    releaseDate: new Date(this.releaseDate).toISOString(),
    imageURL: this.imageURL,
    price: this.price,
    quantityInStock: this.quantityInStock,
    quantitySold: this.quantitySold,
    spec: this.spec
  };
};

mongoose.model("Stock", StockSchema, "Stock");
