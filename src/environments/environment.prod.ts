export const environment = {
  production: true,
  nodeServerURL: "http://localhost:3000/",
  stockImagePath: "http://localhost:3000/" + "img/stock/",
  localstorageShoppingcartKey: "shoppingcart",
  localstorageUserKey: "user",
};
