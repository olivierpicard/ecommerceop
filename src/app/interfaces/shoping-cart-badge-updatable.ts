export interface ShopingCartBadgeUpdatable {
  onShoppingCartBadgeUpdate(number: number);
}
