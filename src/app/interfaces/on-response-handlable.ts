export interface OnResponseHandlable 
{
  onResponseValide(key:string, data)
  onResponseError(key:string, data)
}
