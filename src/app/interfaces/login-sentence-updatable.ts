export interface LoginSentenceUpdatable {
  onLoginStateUpdated(isLogged: boolean, user: any): void
}
