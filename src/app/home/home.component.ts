import { Component, OnInit } from '@angular/core';
import { StockService } from '../services/stock.service';
import { OnResponseHandlable } from '../interfaces/on-response-handlable';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnResponseHandlable {
  bestsellerItems: any;
  latestItems: any;
  
  constructor(
    private stockService: StockService) {}

  ngOnInit() {
    this.stockService.getBestSeller(5, "bestseller", this)
    this.stockService.getLatest(5, "latest", this)
  }

  onResponseValide(key: string, data: any) {
    switch (key) {
      case "bestseller":
        this.bestsellerItems = data    
        break;
      case "latest":
        this.latestItems = data
    }
  }

  onResponseError(key: string, data: any) {
    throw new Error("Method not implemented.");
  }

}
