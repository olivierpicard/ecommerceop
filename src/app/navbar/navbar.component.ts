import { Component, OnInit } from '@angular/core';
import { LoginSentenceUpdatable } from '../interfaces/login-sentence-updatable';
import { LoginService } from '../services/login.service';
import { ShoppingCartService } from '../services/shopping-cart.service';
import { ShopingCartBadgeUpdatable } from '../interfaces/shoping-cart-badge-updatable';
import { stringify } from 'querystring';


@Component({
  selector: 'app-navbar ',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, LoginSentenceUpdatable, ShopingCartBadgeUpdatable {
  
  sentence: string
  shoppingCartItemNumber: number

  constructor(
    private loginService: LoginService,
    private shoppingCartService: ShoppingCartService
  ) { }

  ngOnInit() {
    this.loginService.navbarCallback = this;
    this.shoppingCartService.shoppingCartCallback = this;
    this.shoppingCartItemNumber = this.shoppingCartService.count()
    if(this.loginService.isLogged()) 
      this.onLoginStateUpdated(true, this.loginService.getUser())
    else this.onLoginStateUpdated(false, null)

  }

  logout() {
    this.loginService.logout()
  }

  onLoginStateUpdated(isLogged: boolean, user: any): void {
    this.sentence = (isLogged ? user.username : "Welcome")
  }

  onShoppingCartBadgeUpdate(number: number) {
    console.log("shop : " + number);
    this.shoppingCartItemNumber = number
  }
}
