import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-preview-product',
  templateUrl: './preview-product.component.html',
  styleUrls: ['./preview-product.component.scss']
})
export class PreviewProductComponent implements OnInit {
  @Input() _item;
  title: string = ''
  imageURL: string = ''
  pathImage: string = environment.stockImagePath


  constructor() {}
  ngOnInit() {}

}
