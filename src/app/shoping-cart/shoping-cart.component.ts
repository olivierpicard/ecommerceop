import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../services/shopping-cart.service';
import { environment } from 'src/environments/environment.prod';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shoping-cart',
  templateUrl: './shoping-cart.component.html',
  styleUrls: ['./shoping-cart.component.scss']
})
export class ShopingCartComponent implements OnInit {
  items: any;
  imagePath = environment.stockImagePath

  constructor(
    private shopingCartService: ShoppingCartService,
    private logginService: LoginService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.update()
  }

  private update() {
    this.items = this.shopingCartService.get()
  }

  removeItem(pos) {
    this.shopingCartService.remove(pos)
    this.update()
  }

  order() {
    if(!this.logginService.isLogged) {
      this.router.navigate(["login"])
      return 
    }
    this.router.navigate(["order"])
  }

  totalPrice(): string {
    if(!this.items) return ''
    var total = 0
    this.items.forEach(el => {
      total += Number(el.price)
    });
    return String(total)
  }

  

}
