import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ViewProductComponent } from './view-product/view-product.component';
import { ShopingCartComponent } from './shoping-cart/shoping-cart.component';
import { SignupComponent } from './signup/signup.component';
import { OrderComponent } from './order/order.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'product/:id', component: ViewProductComponent },
  { path: 'shoppingcart', component: ShopingCartComponent },
  { path: 'signup/:backurl', component: SignupComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'order', component: OrderComponent },
  { path: 'login/:backurl', component: LoginComponent },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
