import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-group-products',
  templateUrl: './group-products.component.html',
  styleUrls: ['./group-products.component.scss']
})
export class GroupProductsComponent implements OnInit {
  @Input() _title: string
  @Input() _items: any
  
  constructor() { }

  ngOnInit() {

  }

}
