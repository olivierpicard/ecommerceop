import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { StockService } from '../services/stock.service';
import { OnResponseHandlable } from '../interfaces/on-response-handlable';
import { environment } from 'src/environments/environment.prod';
import { ShoppingCartService } from '../services/shopping-cart.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss']
})
export class ViewProductComponent implements OnInit, OnResponseHandlable {
  item:any;
  spec: any;
  imagePath: string = environment.stockImagePath;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private stockService: StockService,
    private shoppingCart: ShoppingCartService
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.stockService.getById(id, "", this)
  }

  onResponseValide(key: string, data: any) {
    this.item = data
    this.spec = this.item.spec.english;
  }

  onResponseError(key: string, data: any) {}

  onAddToShoppingCart() {
    this.shoppingCart.add(this.item)
  }
}
