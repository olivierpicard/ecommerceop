import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { OnResponseHandlable } from '../interfaces/on-response-handlable';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnResponseHandlable {
  registerForm: FormGroup
  error: string = ""
  backURL: string = "/"

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const url = this.route.snapshot.paramMap.get("backurl")
    this.backURL = (url ? url : "/")
    console.log(this.backURL)
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      password_again: ['', [Validators.required]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit(form) {
    if( this.registerForm.value.password != this.registerForm.value.password_again ) {
      this.error = "les mots de passes ne correspondent pas"
    }
    else if (this.registerForm.invalid) {
      this.error = "Le formulaire est incorrect"
    }
    else {
      this.loginService.signup({
        username: this.registerForm.value.username,
        firstname: this.registerForm.value.firstname,
        lastname: this.registerForm.value.lastname,
        email: this.registerForm.value.email,
        password: this.registerForm.value.password,
      }, "", this)
    }
  }

  onResponseValide(key: string, data: any) {
    this.error = ""
    this.router.navigate([this.backURL]);
  }
  
  onResponseError(key: string, data: any) {
    this.error = "this email already exist"
  }
}
