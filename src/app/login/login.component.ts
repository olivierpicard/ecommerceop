import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  error: string = ""
  backURL: string = "/"

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const url = this.route.snapshot.paramMap.get("backurl")
    this.backURL = (url ? url : "/")
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit(form) {
    if (this.loginForm.invalid) {
      this.error = "Le formulaire est incorrect"
      return 
    }
    this.loginService.login({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    }, "", this)
  }

  onResponseValide(key: string, data: any) {
    this.error = ""
    console.log(this.backURL)
    this.router.navigate([this.backURL]);
  }
  
  onResponseError(key: string, data: any) {
    this.error = "Email doesn't exist"
  }

  onSignup() {
    if(this.backURL === '/')
      this.router.navigate(["signup"])
    else 
      this.router.navigate(["signup/", {backurl: this.backURL}])
  }
}
