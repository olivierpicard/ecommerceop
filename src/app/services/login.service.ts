import { Injectable } from '@angular/core';
import { OnResponseHandlable } from '../interfaces/on-response-handlable';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { LoginSentenceUpdatable } from '../interfaces/login-sentence-updatable';
import { runInThisContext } from 'vm';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public navbarCallback: LoginSentenceUpdatable

  constructor(private http: HttpClient) { }

  public signup(user: any, reqID: string, callback: OnResponseHandlable) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    const body = { user: user }
    this.http.post(environment.nodeServerURL + 'api/users/signup', body, httpOptions)
      .subscribe(data => {
        callback.onResponseValide(reqID, data) 
        localStorage.setItem(environment.localstorageUserKey, JSON.stringify(data))
        this.navbarCallback.onLoginStateUpdated(true, this.getUser())
      }, err => callback.onResponseError(reqID, err))
  }

  public isLogged() : boolean {
    return !!(localStorage.getItem(environment.localstorageUserKey))
  }

  public getUser(): any {
    if(!this.isLogged()) return null
    return JSON.parse(localStorage.getItem(environment.localstorageUserKey))
  }

  public login(user: any, reqID: string, callback: OnResponseHandlable) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }
    const body = { user: user }
    this.http.post(environment.nodeServerURL + 'api/users/login', body, httpOptions)
      .subscribe(data => {
        callback.onResponseValide(reqID, data) 
        localStorage.setItem(environment.localstorageUserKey, JSON.stringify(data))
        console.log("servic logine")
        this.navbarCallback.onLoginStateUpdated(true, this.getUser())
      }, err => callback.onResponseError(reqID, err))
  }

  public logout() {
    localStorage.removeItem(environment.localstorageUserKey)
    this.navbarCallback.onLoginStateUpdated(false, null)
  }


}
