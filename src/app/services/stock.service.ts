import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { OnResponseHandlable } from '../interfaces/on-response-handlable';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private http: HttpClient) { }


  public getBestSeller(limit: number, reqID: string, callback: OnResponseHandlable) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }
    const body = { limit: limit }
    this.http.post(environment.nodeServerURL + 'api/stock/bestseller', body, httpOptions)
      .subscribe(data => callback.onResponseValide(reqID, data), err => callback.onResponseError(reqID, err))
  }


  public getLatest(limit: number, reqID: string, callback: OnResponseHandlable) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }
    const body = { limit: limit }
    this.http.post(environment.nodeServerURL + 'api/stock/latest', body, httpOptions)
      .subscribe(data => callback.onResponseValide(reqID, data), err => callback.onResponseError(reqID, err))
  }

  public getById(id: string, reqID: string, callback: OnResponseHandlable) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }
    const body = { id: id }
    this.http.post(environment.nodeServerURL + 'api/stock/findbyid', body, httpOptions)
      .subscribe(data => callback.onResponseValide(reqID, data), err => callback.onResponseError(reqID, err))
  }

}
