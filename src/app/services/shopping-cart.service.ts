import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { json } from 'express';
import { ShopingCartBadgeUpdatable } from '../interfaces/shoping-cart-badge-updatable';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
public shoppingCartCallback: ShopingCartBadgeUpdatable

  constructor() {}
  
  private createIfNeeded() {
    if(!localStorage.getItem(environment.localstorageShoppingcartKey))
      localStorage.setItem(environment.localstorageShoppingcartKey,
        JSON.stringify([]))
  }

  add(item) {
    const _item = this.get()
    _item.push(item)
    localStorage.setItem(environment.localstorageShoppingcartKey, 
      JSON.stringify(_item))
    this.shoppingCartCallback.onShoppingCartBadgeUpdate(this.count());
  }

  get() : Array<any> {
    this.createIfNeeded()
    return JSON.parse(localStorage.getItem(environment.localstorageShoppingcartKey))
  }

  count() {
    const x = this.get()
    return x.length
  }


  remove(index) {
    const _item = this.get()
    _item.splice(index, 1);
    localStorage.setItem(environment.localstorageShoppingcartKey,
      JSON.stringify(_item))
    this.shoppingCartCallback.onShoppingCartBadgeUpdate(this.count());
  }

  removeAll() {
    localStorage.removeItem(environment.localstorageShoppingcartKey)
    this.shoppingCartCallback.onShoppingCartBadgeUpdate(this.count());
  }
}
