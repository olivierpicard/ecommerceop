import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { ShoppingCartService } from '../services/shopping-cart.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  constructor(
    private serviceLogin: LoginService,
    private shoppingCartService: ShoppingCartService,
    private router: Router
  ) { }

  ngOnInit() {
    if(!this.serviceLogin.isLogged()) {
      this.router.navigate(["login", {backurl: "order"}]);
      return
    }
    this.shoppingCartService.removeAll();
    
  }

}
