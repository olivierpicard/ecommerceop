# eMazing

eMazing is an eCommerce website. 
You need to install node with npm and MongoDB Server installed to run this website  

## Commands


1) You need to install dependences by execute the following command  `npm install`

2) The next step is to start the backend server :  `npm run nodedev`

3) Then you can run the unit tests to check if everythings is correct. Unit test will also populate the data base so you should run this command before accessing this website  `npm run nodetest`

4) Now you need to enable Angular (frontend server) for this you need this command : `ng serve`


Congats, you're done with all setups, you can go to [http://localhost:4200/](http://localhost:4200/) and view the website


